//
//  AlertPopupView.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import Foundation
import UIKit

// MARK: - AlertPopupView
class AlertPopupView {
    
    func showAlert(title: String? = nil, message: String? = nil, buttonTitle: String? = nil) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        
        return alert
    }
}
