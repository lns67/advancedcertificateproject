//
//  NewsModel.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import Foundation

struct NewsResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case articles
        case page
        case pageSize = "page_size"
//        case status
//        case totalHits = "total_hits"
        case totalPages = "total_pages"
        case userInput = "user_input"
    }
    
    let articles: [NewsModel]?
    let page: Int?
    let pageSize: Int?
//    let status: String?
//    let totalHits: Int?
    let totalPages: Int?
    let userInput: UserInput?
}

struct NewsModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case id = "_id"
        case score = "_score"
        case author = "author"
        case media
        case publishedDate = "published_date"
        case description = "summary"
        case title
    }
    
    let id: String?
    let score: Double?
    let author: String?
    let media: String?
    let publishedDate: String?
    let description: String?
    let title: String?
}

struct UserInput: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case fromDate = "from"
        case language = "lang"
        case page
        case q
        case size
        case sortBy = "sort_by"
    }
    
    let fromDate: String?
    let language: String?
    let page: Int?
    let q: String?
    let size: Int?
    let sortBy: String?
}

struct Pagination {
    
    var page: Int
    var pageSize: Int
    var totalPages: Int
}
