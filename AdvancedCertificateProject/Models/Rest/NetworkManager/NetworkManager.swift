//
//  NetworkManager.swift
//  AdvancedCertificateProject
//
//  Created by DS on 24.11.2021.
//

import Foundation
import UIKit
import Alamofire

// MARK: - NetworkManager
class NetworkManager {
    
    static let shared = NetworkManager()
    
    let apiHost = "free-news.p.rapidapi.com"
    let apiKey = "078330417bmsh94a5cd2f513d694p1616e8jsn59bf695086c9"
    
    func networkRequest(parametrs: [String : Any],
                        method: HTTPMethod,
                        headers: [String : String]? = nil,
                        urlPath: String,
                        completionHandler: @escaping (Data?, String?) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable ?? false {
            
            var staticHTTPHeaders: [String : String] = ["X-RapidAPI-Host" : apiHost,
                                                        "X-RapidAPI-Key" : apiKey]
            var httpHeaders: [HTTPHeader] = []
            if let headers = headers {
                for (key, value) in headers {
                    staticHTTPHeaders[key] = value
                }
            }
            
            staticHTTPHeaders.forEach { (key, value) in
                let httpHeader = HTTPHeader(name: key, value: value)
                httpHeaders.append(httpHeader)
            }
            
            AF.request(URLs.newsApi,
                       method: method,
                       parameters: parametrs,
                       headers: HTTPHeaders(httpHeaders)).response { response in
                
                switch response.result {
                case .success(let data):
                    completionHandler(data, nil)
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
            }
            
        } else {
            completionHandler(nil, "No internet connection")
        }
    }
}
