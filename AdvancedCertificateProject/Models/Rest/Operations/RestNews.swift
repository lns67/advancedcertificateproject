//
//  RestNews.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import Foundation

// MARK: - RestNews
class RestNews{
    
    func loadNews(search: String, pagination: Pagination, completionHandler: @escaping (NewsResponse?, String?) -> Void) {
        
        let parametrs: [String : Any] = ["q" : search, "page" : pagination.page, "page_size" : pagination.pageSize]
        
        NetworkManager.shared.networkRequest(parametrs: parametrs, method: .get, urlPath: URLs.newsApi) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode(NewsResponse.self, from: data)
            completionHandler(response, nil)
        }
    }
}
