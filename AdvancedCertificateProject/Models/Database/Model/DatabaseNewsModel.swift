//
//  DatabaseNewsModel.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import Foundation
import Realm
import RealmSwift

class DatabaseNewsResponse: Object {
    
    var articles: List<DatabaseNewsModel> = List<DatabaseNewsModel>()
    @objc dynamic var page: Int = 0
    @objc dynamic var pageSize: Int = 0
    @objc dynamic var status: String?
    @objc dynamic var totalHits: Int = 0
    @objc dynamic var totalPages: Int = 0
    var userInput: DatabaseUserInput? = DatabaseUserInput()
}

class DatabaseNewsModel: Object {
    
    @objc dynamic var id: String?
    @objc dynamic var score: Double = 0
    @objc dynamic var author: String?
    @objc dynamic var media: String?
    @objc dynamic var publishedDate: String?
    @objc dynamic var descriptionNews: String?
    @objc dynamic var title: String?
}

class DatabaseUserInput: Object {
    
    @objc dynamic var fromDate: String?
    @objc dynamic var language: String?
    @objc dynamic var page: Int = 0
    @objc dynamic var q: String?
    @objc dynamic var size: Int = 0
    @objc dynamic var sortBy: String?
}
