//
//  DatabaseManager.swift
//  AdvancedCertificateProject
//
//  Created by DS on 24.11.2021.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    private let realm = try? Realm()
    
    func saveData(newsResponse: NewsResponse) {
        
        let databaseNews = transformDatabaseElement(newsResponse: newsResponse)
        
        do {
            
            try realm?.write {
                realm?.add(databaseNews)
            }
        } catch(let error) {
            print(error.localizedDescription)
        }
    }
    
    func getData(completionHadler: @escaping(NewsResponse?) -> Void) {
        
        guard let databaseNews = realm?.objects(DatabaseNewsModel.self) else {
            
            completionHadler(nil)
            return
        }
        
        completionHadler(transformElement(databaseNews: databaseNews))
    }
    
    // MARK: - transform element from usual to Database
    private func transformDatabaseElement(newsResponse: NewsResponse) -> DatabaseNewsResponse {
        
        let databaseNewsResponse: DatabaseNewsResponse = DatabaseNewsResponse()
        
        let databaseArticlesList: List<DatabaseNewsModel> = List<DatabaseNewsModel>()
        
        if let articles = newsResponse.articles {
            for newsArticle in articles {
                
                let databaseNewsModel: DatabaseNewsModel = DatabaseNewsModel()
                            
                databaseNewsModel.id = newsArticle.id
                databaseNewsModel.title = newsArticle.title
                databaseNewsModel.media = newsArticle.media
                databaseNewsModel.author = newsArticle.author
                databaseNewsModel.descriptionNews = newsArticle.description
                databaseNewsModel.score = newsArticle.score ?? 0
                databaseNewsModel.publishedDate = newsArticle.publishedDate
                                        
                databaseArticlesList.append(databaseNewsModel)
                
                databaseNewsResponse.articles = databaseArticlesList
            }
        }
        
        databaseNewsResponse.page = newsResponse.page ?? 0
        databaseNewsResponse.pageSize = newsResponse.pageSize ?? 0
        databaseNewsResponse.totalPages = newsResponse.totalPages ?? 0
        
        return databaseNewsResponse
    }
    
    // MARK: - transform element from Database to usual
    private func transformElement(databaseNews: Results<DatabaseNewsModel>) -> NewsResponse {
        var currenciesArray: [NewsModel] = []
        
        for databaseNews in databaseNews {
            
            if let id = databaseNews.id, let author = databaseNews.author, let media = databaseNews.media, let publishedDate = databaseNews.publishedDate, let descriptionNews = databaseNews.descriptionNews, let title = databaseNews.title {
                
                
                let currencyManager = NewsModel(id: id, score: databaseNews.score, author: author, media: media, publishedDate: publishedDate, description: descriptionNews, title: title)
                
                currenciesArray.append(currencyManager)
            }
        }
        
        let listOfCurrenciesExchanges: NewsResponse = NewsResponse(articles: currenciesArray, page: 1, pageSize: currenciesArray.count, totalPages: currenciesArray.count, userInput: UserInput(fromDate: "", language: "", page: 0, q: "", size: 0, sortBy: ""))
        
        return listOfCurrenciesExchanges
    }
}
