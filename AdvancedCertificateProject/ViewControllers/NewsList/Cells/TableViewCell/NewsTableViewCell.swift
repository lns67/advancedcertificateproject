//
//  NewsTableViewCell.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        descriptionLabel.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(news: NewsModel) {
        
        titleLabel.text = news.title
//        descriptionLabel.text = news.description
        
        if let imageUrl = URL(string: news.media ?? "") {
            newsImage.kf.setImage(with: imageUrl)
        }
    }
}
