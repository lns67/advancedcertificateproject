//
//  NewsListViewController+Extensions.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import Foundation
import UIKit

// MARK: - UITableViewDelegate, UITableViewDataSource
extension NewsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        newsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: newsCellID, for: indexPath) as! NewsTableViewCell
        cell.update(news: newsList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let newsInfoVc = NewsInfoViewController(nibName: "NewsInfoViewController", bundle: nil)
        newsInfoVc.newsArticle = newsList[indexPath.row]
        navigationController?.pushViewController(newsInfoVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == newsList.count - 1 && newsList.count < pagination.totalPages {
            pagination.page += 1
            loadNews()
        }
    }
}

// MARK: - Private Methods
extension NewsListViewController {
    
    func loadNews() {
        
        guard let text = searchBar.text, !text.isEmpty else { return }
        
        RestNews().loadNews(search: text, pagination: pagination) { response, error in
            
            guard error == nil else {
                
                self.present(AlertPopupView().showAlert(title: "Error", message: error, buttonTitle: "Ok"), animated: true, completion: nil)
                return
            }
            
            guard let response = response else {
                
                self.present(AlertPopupView().showAlert(title: "Error", message: error, buttonTitle: "Ok"), animated: true, completion: nil)
                return
            }

            self.pagination = Pagination(page: response.page ?? 0, pageSize: response.pageSize ?? 0, totalPages: response.totalPages ?? 0)
            self.newsList += response.articles ?? []
            
            DatabaseManager.shared.saveData(newsResponse: response)
            
            DispatchQueue.main.async {
                self.newsListTableView.reloadData()
            }
        }
    }
}

extension NewsListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        pagination = Pagination(page: 1, pageSize: 10, totalPages: 1)
        newsList = []
        loadNews()
    }
}
