//
//  NewsListViewController.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import UIKit
import Alamofire

// MARK: - NewsListViewController
class NewsListViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var newsListTableView: UITableView!
    
    var newsList: [NewsModel] = []
    var pagination = Pagination(page: 1, pageSize: 10, totalPages: 0)
    let newsCellID = String(describing: NewsTableViewCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsListTableView.register(UINib(nibName: newsCellID, bundle: nil), forCellReuseIdentifier: newsCellID)
        newsListTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if NetworkReachabilityManager()?.isReachable ?? false {
            
            loadNews()
            
        } else {
            
            DatabaseManager.shared.getData(completionHadler: { response in
                
                guard let response = response else { return }
                
                self.newsList = response.articles ?? []
            })
        }
    }
}
