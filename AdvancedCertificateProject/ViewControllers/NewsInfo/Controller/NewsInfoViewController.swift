//
//  NewsInfoViewController.swift
//  AdvancedCertificateProject
//
//  Created by DS on 27.11.2021.
//

import UIKit
import Kingfisher

// MARK: - NewsInfoViewController
class NewsInfoViewController: UIViewController {

    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    var newsArticle: NewsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let newsArticle = newsArticle {
            
            newsTitleLabel.text = newsArticle.title
            descriptionLabel.text = newsArticle.description
            
            if let imageUrl = URL(string: newsArticle.media ?? "") {
                newsImage.kf.setImage(with: imageUrl)
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
